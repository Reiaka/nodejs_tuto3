var config = require('../config/database');
var passport = require('passport');

require('../config/passport')(passport);
var jwt = require('jsonwebtoken');
var User = require("../models/users.models");
//var Book = require("../models/books.models");


//const Users = require('../models/users.models');

const controller = {
    addUser:(req,res,next)=>{
        if (!req.body.username || !req.body.password) {
            res.json({success: false, msg: 'Please pass username and password.'});
          } else {
            var newUser = new User({
              username: req.body.username,
              password: req.body.password
            });
            // save the user
            newUser.save(function(err) {
              if (err) {
                return res.json({success: false, msg: 'Username already exists.'});
              }
              res.json({success: true, msg: 'Successful created new user.'});
            });
          }
    },
    getUser:(req,res,next)=>{
        User.findOne({
            username: req.body.username
          }, function(err, user) {
            if (err) throw err;
        
            if (!user) {
              res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
            } else {
              // check if password matches
              user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                  // if user is found and password is right create a token
                  var token = jwt.sign(user.toJSON(), config.secret);
                  // return the information including token as JSON
                  res.json({success: true, token: 'JWT ' + token});
                } else {
                  res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
                }
              });
            }
          });
    }
};

module.exports = controller;