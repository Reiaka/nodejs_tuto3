const Books = require('../models/books.models');

const controller = {
    getAll:(req,res, next)=>{
       Books.find({})
       .then(
           books =>{
            res.statusCode = 200;
            res.setHeader('Content-Type','application/json');
            res.json(books);        
           },
           err => next(err)
       )
       .catch(err => next(err));
    },

    addOne:(req,res, next)=>{
        Books.create(req.body)
        .then(
            book=>{
                res.statusCode = 200;
                res.setHeader('Content-Type','application/json');
                res.json(book);
            },
            err => next(err)
       )
       .catch(err => next(err));
    },

    getOne:(req,res, next)=>{
        Books.findById(req.params.bookId)
        .then(
            book =>{
            res.statusCode = 200;
            res.setHeader('Content-Type','application/json');
            res.json(book);
            },
            err => next(err)
            )
            .catch(err => next(err));
    },

    updateOne:(req,res, next)=>{
        Books
        .findByIdAndUpdate(
            req.params.bookId,
            {$set:req.body},
            {new:true}
        )
        .then(
            book =>{
                res.statusCode = 200;
                res.setHeader('Content-Type','application/json');
                res.json(book);
                },
                err => next(err)
                )
                .catch(err => next(err));
    },

    deleteOne:(req,res, next)=>{
        Book.findByIdAndRemove(req.params.bookId)
        .then(
            book =>{
                res.statusCode = 200;
                res.setHeader('Content-Type','application/json');
                res.json(book);
            },
            err => next(err)
        )
        .catch(
            err => next(err)
        );
    },

    getAllComments: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                book => {
                    res.statusCode = 200;
                    res.setHeader("Content-type", "application/json");
                    res.json(book.comments);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    addOneComment: (req, res, next) => {
        const newComment = {
            rating:req.body.rating,
            comment:req.body.comment,
            author:req.body.author
        };
        Books.findById(req.params.bookId)
            .then(
                book => {
                    book.comments.push(newComment);
                    book.save();
                    res.statusCode = 200;
                    res.setHeader("Content-type", "application/json");
                    res.json(book);
                }
            )
    },

    getOneComment: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                book => {
                    var comment = book.comments.id(req.params.commentId);
                    res.statusCode = 200;
                    res.setHeader("Content-type", "application/json");
                    res.json(comment);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    updateOneComment: (req, res, next) => {
        const newComment = {
            rating:req.body.rating,
            comment:req.body.comment,
            author:req.body.author
        };
        Books.findById(req.params.bookId)
            .then(
                book => {
                    var comment = book.comments.id(req.params.commentId);
                    comment.set(newComment);
                    book.save();
                    res.statusCode = 200;
                    res.setHeader("Content-type", "application/json");
                    res.json(comment);
                },
                err => next(err)
            )
            .catch(err => next(err));
    },

    deleteOneComment: (req, res, next) => {
        Books.findById(req.params.bookId)
            .then(
                book => {
                    var comment = book.comments.id(req.params.commentId);
                    comment.remove();
                    book.save();
                    res.statusCode = 200;
                    res.setHeader("Content-type", "application/json");
                    res.json(book);
                },
                err => next(err)
            )
            .catch(err => next(err));
    }
};

module.exports = controller;