const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

var commentSchema = new Schema({
    rating:{type:Number,min:1,max:5,required:true},
    comment:{type:String,required:true},
    author:{type:mongoose.Schema.Types.ObjectId,ref:'User'}
    },
    {timestamps:true}
);

const bookSchema = new Schema(
    {title: {type:String, required:true},
    author:{type:String, required:true},
    image:{type:String},
    year:{type:Number},
    price:{type:Currency},
    bestseller:{type:Boolean,default:false},
    comments:[commentSchema]
    },
    {timestamps:true}
);

const Books = mongoose.model('Book',bookSchema);

module.exports = Books;