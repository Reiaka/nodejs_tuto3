require('jsonwebtoken');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');


const userSchema = new Schema({
    username:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    first_name:{type:String},
    last_name:{type:String},
    admin:{type:Boolean}
})

userSchema.pre('save',function(next){
    var user = this;
    if (this.isModified('password')||this.isNew){
        bcrypt.genSalt(10,function(err,salt){
            if(err){
                return next(err);
            }
            bcrypt.hash(user.password,salt,null, function(err,hash){
                if(err){
                    return next(err);
                }
                user.password = hash;
                next();
            });
        }); 
    }
    else{
        return next();
    }
});

userSchema.methods.comparePassword = function(passw,cb){
    bcrypt.compare(passw,this.password,function(err,isWatch){
        if (err){
            return cb(err);
        }
        cb(null,isWatch);
    });
};

const Users = mongoose.model('Users',userSchema);

module.exports = Users;