var express = require('express');

var router = express.Router();


const controller = require('../controllers/users.controller');

router.use(express.json());

router
.route('/signup')
  .post(controller.addUser);

router
.route('/login')
  .post(controller.getUser);


module.exports = router;
