const controller = require('../controllers/books.controller');
const express = require('express');
var passport = require('passport');

const router = express.Router();

getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};
router.use(express.json());

router
.route('/')
    .get(passport.authenticate('jwt', { session: false }), function (req, res, next){
        var token = getToken(req.headers);
        if(token){
            controller.getAll(req,res,next);
        }
        else{
    return res.status(403).send({ success: false, msg: 'Unauthorized.2' });
        }
    }
        )
    .post(controller.addOne);

router
.route('/:bookId')
    .get(controller.getOne)
    .put(controller.updateOne)
    .delete(controller.deleteOne);

router
.route('/:bookId/comments')
    .get(controller.getAllComments)
    .post(controller.addOneComment);

router
.route('/:bookId/comments/:commentId')
    .get(controller.getOneComment)
    .put(controller.updateOneComment)
    .delete(controller.deleteOneComment);

module.exports = router;