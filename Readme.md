Initiation à Node & Express JS
==

# Node ?

Node.js est une plateforme logicielle libre et événementielle en JavaScript orientée vers les applications réseau qui doivent pouvoir monter en charge. Elle utilise la machine virtuelle V8 et implémente sous licence MIT les spécifications CommonJS.

# Express JS ?

Express.js est un framework pour construire des applications web basées sur Node.js. C'est de fait le framework standard pour le développement de serveur en Node.js. L'auteur original, TJ Holowaychuck, le décrit comme un serveur inspiré de Sinatra dans le sens qu'il est relativement minimaliste tout en permettant d'étendre ses fonctionnalités via des plugins.

# Installer Node

#### Update du systéme
`sudo apt-get update`

#### Installer Node & Npm
`sudo apt-get install nodejs npm`

# Initialiser un projet Node Basic
`mkdir node-express`

`cd node-express`

`npm init`

# Initialiser un projet Express JS

#### Installer Express JS
`npm install express-generator -g`

#### Créer le projet
`express myapp`

`cd myapp`

`npm install`

# Lancer le serveur
Ajouter "start": "node index.js", dans "scripts" dans le fichier package.json
`npm start`

# Installer morgan
`npm install morgan --save`

# Installer l'ODM Mongoose
`npm install mongoose --save`

#### Installer MongoDB (Debian, Ubuntu & derivées)
`sudo apt-get install mongodb`

#### Activer MongoDB comme service (Daemon)
`sudo systemctl start mongodb`

# Initialiser une Base de données MonngoDB
`use amazon`

`db.books.insert({title:"Harry Potter and the Goblet of Fire", author:"J.K.Rowling"})`

`show collections`

`db.books.find({}).pretty()`

#### Vider la base de données

`db.books.drop()`

##### Se connecter à la BDD

```
var mongoose = require('mongoose');

const url = 'mongodb://localhost:27017/amazon';
const connect = mongoose.connect(url, {useNewUrlParser: true});
connect.then(
  db => {
    console.log("Connected correctly to the server");
  },
  err => {
    console.log(err);
  }
);

```

# Activer le support de l'HTTPS

#### Générer un certificat auto-signé
`openssl req -nodes -new -x509 -keyout server.key -out server.cert`

#### Activer le support du HTTPS
```
var https = require('https');

var server = https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
}, app);

```

# Installer Mongoose-currency
`npm install mongoose-currency --save`

# Installer Passport JS
`npm install passport`

`npm install passport-jwt`

`npm install passport-local`

`npm install bcrypt-nodejs`
